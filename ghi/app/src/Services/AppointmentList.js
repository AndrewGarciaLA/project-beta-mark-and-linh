import { React, useState, useEffect } from "react"

export default function AppointmentList() {
    const [appointmentList, setAppointmentList] = useState([]);
    const [vin, setVin] = useState('')


    const fetchData = async () => {
        const url = "http://localhost:8080/api/appointments/"
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setAppointmentList(data.appointments)
        }
    }

    const fetchAutomobileData = async () => {
        const url = "http://localhost:8080/api/automobiles/"
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            let sold = []
            for (const auto of data.autos) {
                if (auto.sold === true) {
                    sold.push(auto.vin)
                }
            }
            setVin(sold)
        }
    }

    const vip = (vinNumber) => (vin.includes(vinNumber) ? 'yes :)' : 'no :(')

    useEffect(() => {
        fetchData()
        fetchAutomobileData()
    }, [])

    const handleCancel = async (id) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, {
            method: 'put',
            body: JSON.stringify({ status: 'cancelled' }),
            headers: { 'Content-Type': 'application/json' },
        })
        if (response.ok) {
            fetchData();
        }
    }

    const handleFinish = async (id) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, {
            method: 'put',
            body: JSON.stringify({ status: 'finished' }),
            headers: { 'Content-Type': 'application/json' },
        })
        if (response.ok) {
            fetchData();
        }
    }

    return (
        <div>
            <div>
                <h1>Service Appointments</h1>
            </div>
            <table className="table table-hover table-striped border border-5">
                <thead>
                    <tr>
                        <th className='text-center'>Date Time</th>
                        <th className='text-center'>Customer</th>
                        <th className='text-center'>VIN</th>
                        <th className='text-center'>Reason</th>
                        <th className='text-center'>Status</th>
                        <th className='text-center'>Technician</th>
                        <th className='text-center'>Cancel?</th>
                        <th className='text-center'>Finish?</th>
                        <th className="text-center">VIP</th>
                    </tr>
                </thead>
                <tbody>
                    {appointmentList.map(appointment => {
                        return (
                            <tr key={appointment.id}>
                                <td className='text-center'>{appointment.date_time}</td>
                                <td className='text-center'>{appointment.customer}</td>
                                <td className='text-center'>{appointment.vin}</td>
                                <td className='text-center'>{appointment.reason}</td>
                                <td className='text-center'>{appointment.status}</td>
                                <td className='text-center'>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                <td>
                                    <button type="button" className="btn btn-warning" onClick={() => handleCancel(appointment.id)}>cancel</button>
                                </td>
                                <td>
                                    <button type="button" className="btn btn-success" onClick={() => handleFinish(appointment.id)}>finish</button>
                                </td>
                                <td className="text-center">{vip(appointment.vin)}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )


}